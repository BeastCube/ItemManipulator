ItemManipulator
========

[![Build Status](http://ci.patzleiner.net/job/ItemManipulator/badge/icon)](http://ci.patzleiner.net/job/ItemManipulator/)

With the ItemManipulator you can manipulate the item you are holding in your hand.

The following commands are available:

- /im material \<material>
- /im amount \<amount>
- /im durability \<durability>
- /im data \<data> (Deprecated, use `/im durability <durability>` instead)
- /im displayname \<displayname>
- /im lore \<lore>
- /im clearlores
- /im enchantment \<enchantment> [\<level>]
- /im clearenchantments
- /im color \<color>
- /im woolcolor \<woolcolor>
- /im skullowner \<name>
- /im addflag \<flag>
- /im removeflag \<flag>
- /im addallflags
- /im removeallflags

Players need the permission `itemmanipulator` or need to be OP to use this command