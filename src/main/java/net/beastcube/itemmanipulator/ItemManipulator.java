package net.beastcube.itemmanipulator;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ItemManipulator extends JavaPlugin {

    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("im")) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (p.hasPermission("itemmanipulator") || p.isOp()) {
                    if (args.length > 0) {
                        ItemStack stack = p.getItemInHand();
                        if (stack.getType() != null && stack.getType() != Material.AIR) {
                            String action = args[0];
                            if (action.equalsIgnoreCase("material")) {
                                if (args.length > 1) {
                                    Material mat = Material.getMaterial(args[1].toUpperCase());
                                    if (mat == null) {
                                        try {
                                            mat = Material.getMaterial(Integer.parseInt(args[1]));
                                        } catch (Exception ignored) {
                                        }
                                    }
                                    if (mat == null) {
                                        sender.sendMessage(ChatColor.RED + "This material does not exist!");
                                        return true;
                                    }
                                    stack.setType(mat);
                                    p.sendMessage(ChatColor.GREEN + "Successfully changed the material to " + mat.name());
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im material <material>");
                                }
                            } else if (action.equalsIgnoreCase("amount")) {
                                if (args.length > 1) {
                                    int amount;
                                    try {
                                        amount = Integer.parseInt(args[1]);
                                    } catch (Exception ex) {
                                        sender.sendMessage(ChatColor.RED + "That wasn't a number was it?");
                                        return true;
                                    }
                                    stack.setAmount(amount);
                                    p.sendMessage(ChatColor.GREEN + "Successfully changed the amount to " + amount);
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im amount <amount>");
                                }
                            } else if (action.equalsIgnoreCase("durability")) {
                                if (args.length > 1) {
                                    short d;
                                    try {
                                        d = Short.parseShort(args[1]);
                                    } catch (Exception ex) {
                                        sender.sendMessage(ChatColor.RED + "That wasn't a number was it?");
                                        return true;
                                    }
                                    stack.setDurability(d);
                                    p.sendMessage(ChatColor.GREEN + "Successfully changed the durability to " + d);
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im durability <durability>");
                                }
                            } else if (action.equalsIgnoreCase("data")) {
                                if (args.length > 1) {
                                    short d;
                                    try {
                                        d = Short.parseShort(args[1]);
                                    } catch (Exception ex) {
                                        sender.sendMessage(ChatColor.RED + "That wasn't a number was it?");
                                        return true;
                                    }
                                    p.setItemInHand(new ItemBuilder(stack).data(d).build());
                                    p.sendMessage(ChatColor.GREEN + "Successfully set the data");
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im data <data>");
                                }
                            } else if (action.equalsIgnoreCase("displayname")) {
                                if (args.length > 1) {
                                    List<String> name = new LinkedList<>(Arrays.asList(args));
                                    name.remove(0);
                                    String displayname = ChatColor.translateAlternateColorCodes('&', join(" ", name.toArray(new String[name.size()])));
                                    p.setItemInHand(new ItemBuilder(stack).name(displayname).build());
                                    p.sendMessage(ChatColor.GREEN + "Successfully changed the display name to " + ChatColor.RESET + ChatColor.translateAlternateColorCodes('&', displayname));
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im displayname <displayname>");
                                }
                            } else if (action.equalsIgnoreCase("lore")) {
                                if (args.length > 1) {
                                    List<String> name = new LinkedList<>(Arrays.asList(args));
                                    name.remove(0);
                                    String lore = ChatColor.translateAlternateColorCodes('&', join(" ", name.toArray(new String[name.size()])));
                                    p.setItemInHand(new ItemBuilder(stack).lore(lore).build());
                                    p.sendMessage(ChatColor.GREEN + "Successfully added lore");
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im lore <lore>");
                                }
                            } else if (action.equalsIgnoreCase("clearlores")) {
                                p.setItemInHand(new ItemBuilder(stack).clearLore().build());
                                p.sendMessage(ChatColor.GREEN + "Successfully cleared lores");
                            } else if (action.equalsIgnoreCase("enchantment")) {
                                if (args.length > 1) {
                                    Enchantment ent = Enchantment.getByName(args[1]);
                                    if (ent == null) {
                                        try {
                                            ent = Enchantment.getById(Integer.parseInt(args[1]));
                                        } catch (Exception ignored) {
                                        }
                                    }
                                    if (ent == null) {
                                        sender.sendMessage(ChatColor.RED + "This enchantment does not exist!");
                                        List<String> enchantments = new ArrayList<>();
                                        for (Enchantment ench : Enchantment.values()) {
                                            enchantments.add(ench.getName());
                                        }
                                        sender.sendMessage(join(", ", enchantments.toArray(new String[enchantments.size()])));
                                        return true;
                                    }
                                    if (args.length > 2) {
                                        try {
                                            p.setItemInHand(new ItemBuilder(stack).enchantment(ent, Integer.parseInt(args[2])).build());
                                        } catch (Exception ex) {
                                            sender.sendMessage(ChatColor.RED + "That wasn't a number was it?");
                                            return true;
                                        }
                                    } else {
                                        p.setItemInHand(new ItemBuilder(stack).enchantment(ent).build());
                                    }
                                    p.sendMessage(ChatColor.GREEN + "Successfully added enchantment");
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im enchantment <enchantment> [<level>]");
                                }
                            } else if (action.equalsIgnoreCase("clearenchantments")) {
                                p.setItemInHand(new ItemBuilder(stack).clearEnchantments().build());
                                p.sendMessage(ChatColor.GREEN + "Successfully cleared enchantments");
                            } else if (action.equalsIgnoreCase("color")) {
                                try {
                                    Color color = null;
                                    try {
                                        if (args.length == 2) {
                                            color = Color.fromRGB(Integer.parseInt(args[1]));
                                        } else if (args.length == 4) {
                                            color = Color.fromRGB(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
                                        } else {
                                            p.sendMessage(ChatColor.RED + "Usage: /im color <red> <green> <blue>");
                                        }
                                    } catch (Exception ex) {
                                        sender.sendMessage(ChatColor.RED + "That wasn't a number was it?");
                                        return true;
                                    }
                                    if (color == null) {
                                        sender.sendMessage(ChatColor.RED + "Invalid color");
                                        return true;
                                    }
                                    p.setItemInHand(new ItemBuilder(stack).color(color).build());
                                    p.sendMessage(ChatColor.GREEN + "Successfully set the color");
                                } catch (IllegalArgumentException ex) {
                                    p.sendMessage(ChatColor.RED + "/im color is only applicable for leather armor!");
                                }
                            } else if (action.equalsIgnoreCase("woolcolor")) {
                                if (args.length > 1) {
                                    try {
                                        DyeColor dyecolor;
                                        try {
                                            dyecolor = DyeColor.valueOf(args[1].toUpperCase());
                                        } catch (IllegalArgumentException ignored) {
                                            dyecolor = DyeColor.getByDyeData(Byte.parseByte(args[1]));
                                        }
                                        if (dyecolor == null) {
                                            sender.sendMessage(ChatColor.RED + "This woolcolor does not exist!");
                                            List<String> colors = new ArrayList<>();
                                            for (DyeColor color : DyeColor.values()) {
                                                colors.add(color.name());
                                            }
                                            sender.sendMessage(join(", ", colors.toArray(new String[colors.size()])));
                                            return true;
                                        }
                                        p.setItemInHand(new ItemBuilder(stack).woolColor(dyecolor).build());
                                        p.sendMessage(ChatColor.GREEN + "Successfully set the woolcolor");
                                    } catch (IllegalArgumentException ex) {
                                        p.sendMessage(ChatColor.RED + "/im woolcolor is only applicable for wool!");
                                    }
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im woolcolor <woolcolor>");
                                }
                            } else if (action.equalsIgnoreCase("skullowner")) {
                                if (args.length > 1) {
                                    try {
                                        p.setItemInHand(new ItemBuilder(stack).skullOwner(args[1]).build());
                                        p.sendMessage(ChatColor.GREEN + "Successfully set the woolcolor");
                                    } catch (IllegalArgumentException ex) {
                                        p.sendMessage(ChatColor.RED + "/im skullowner is only applicable for skulls!");
                                    }
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im skullowner <name>");
                                }
                            } else if (action.equalsIgnoreCase("addflag")) {
                                if (args.length > 1) {
                                    ItemFlag flag = null;
                                    try {
                                        flag = ItemFlag.valueOf(args[1].toUpperCase());
                                    } catch (IllegalArgumentException ignored) {
                                    }
                                    if (flag == null) {
                                        sender.sendMessage(ChatColor.RED + "This flag does not exist!");
                                        List<String> flags = new ArrayList<>();
                                        for (ItemFlag itemflag : ItemFlag.values()) {
                                            flags.add(itemflag.name());
                                        }
                                        sender.sendMessage(join(", ", flags.toArray(new String[flags.size()])));
                                        return true;
                                    }
                                    p.setItemInHand(new ItemBuilder(stack).addFlag(flag).build());
                                    p.sendMessage(ChatColor.GREEN + "Successfully added flag");
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im addflag <flag>");
                                }
                            } else if (action.equalsIgnoreCase("removeflag")) {
                                if (args.length > 1) {
                                    ItemFlag flag = null;
                                    try {
                                        flag = ItemFlag.valueOf(args[1].toUpperCase());
                                    } catch (IllegalArgumentException ignored) {
                                    }
                                    if (flag == null) {
                                        sender.sendMessage(ChatColor.RED + "This flag does not exist!");
                                        List<String> flags = new ArrayList<>();
                                        for (ItemFlag itemflag : ItemFlag.values()) {
                                            flags.add(itemflag.name());
                                        }
                                        sender.sendMessage(join(", ", flags.toArray(new String[flags.size()])));
                                        return true;
                                    }
                                    p.setItemInHand(new ItemBuilder(stack).removeFlag(flag).build());
                                    p.sendMessage(ChatColor.GREEN + "Successfully removed flag");
                                } else {
                                    p.sendMessage(ChatColor.RED + "Usage: /im removeflag <flag>");
                                }
                            } else if (action.equalsIgnoreCase("addallflags")) {
                                p.setItemInHand(new ItemBuilder(stack).addAllFlags().build());
                                p.sendMessage(ChatColor.GREEN + "Successfully added all flags");
                            } else if (action.equalsIgnoreCase("removeallflags")) {
                                p.setItemInHand(new ItemBuilder(stack).removeAllFlags().build());
                                p.sendMessage(ChatColor.GREEN + "Successfully removed all flags");
                            } else {
                                showHelp(p);
                            }
                        } else {
                            p.sendMessage(ChatColor.RED + "You don't have any item in your hand");
                        }
                    } else {
                        showHelp(p);
                    }
                } else {
                    p.sendMessage(ChatColor.RED + "You don't have permission to use this command!");
                }
            } else {
                sender.sendMessage(ChatColor.RED + "Please execute this command as player");
            }
            return true;
        } else {
            return false;
        }
    }

    private void showHelp(Player p) {
        p.sendMessage(ChatColor.GOLD + "~~~~~~ItemManipulator~~~~~~");
        p.sendMessage("/im material <material>");
        p.sendMessage("/im amount <amount>");
        p.sendMessage("/im durability <durability>");
        p.sendMessage("/im data <data>");
        p.sendMessage("/im displayname <displayname>");
        p.sendMessage("/im lore <lore>");
        p.sendMessage("/im clearlores");
        p.sendMessage("/im enchantment <enchantment> [<level>]");
        p.sendMessage("/im clearenchantments");
        p.sendMessage("/im color <color>");
        p.sendMessage("/im woolcolor <woolcolor>");
        p.sendMessage("/im skullowner <name>");
        p.sendMessage("/im addflag <flag>");
        p.sendMessage("/im removeflag <flag>");
        p.sendMessage("/im addallflags");
        p.sendMessage("/im removeallflags");
    }

    public static String join(String separator, String... strings) {
        String str = "";
        boolean space = false;
        for (String a : strings) {
            str = str + (space ? separator : "") + a;
            space = true;
        }
        return str;
    }

}
